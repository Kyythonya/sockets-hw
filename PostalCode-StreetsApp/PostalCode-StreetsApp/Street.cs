﻿namespace PostalCode_StreetsApp
{
    public class Street
    {
        public string PostCode { get; set; }

        public string Name { get; set; }
    }
}
