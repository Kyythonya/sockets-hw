﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;

namespace PostalCode_StreetsWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int BUFFER_SIZE = 1024 * 4;
        private byte[] _buffer = new byte[BUFFER_SIZE];

        private readonly string _ipAddress = "192.168.56.1";
        private readonly int _port = 10001;

        private Socket _clientSocket;

        public MainWindow()
        {
            InitializeComponent();

            try
            {
                _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void GetStreetsButtonClick(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem((state) =>
            {
                try
                {
                    if (!_clientSocket.Connected)
                        _clientSocket.Connect(IPAddress.Parse(_ipAddress), _port);

                    Dispatcher.Invoke(() =>
                    {
                        _buffer = Encoding.ASCII.GetBytes(postalCodeTextBox.Text);
                    });
                    _clientSocket.Send(_buffer);

                    _buffer = new byte[BUFFER_SIZE];

                    int receivedSize = _clientSocket.Receive(_buffer);
                    if (receivedSize > 0)
                    {
                        Dispatcher.Invoke(new Action(() =>
                        {
                            streetsTextBox.Text = Encoding.UTF8.GetString(_buffer, 0, receivedSize);
                        }));
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            });
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _clientSocket.Shutdown(SocketShutdown.Both);
            _clientSocket.Close();
        }
    }
}
